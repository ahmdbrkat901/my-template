import React, { useState } from 'react'
import logo from '../../imgs/logo.png'
import './sidbar.css'
import { SideData } from '../../Data/data'
import { motion } from "framer-motion";
import { UilSignOutAlt, UilBars } from '@iconscout/react-unicons'

const Sidbar = () => {
    const [selected, setSelected] = useState(0)
    const [expanded, setExpaned] = useState(true)
    const sidebarVariants = {
        true: {
            left: '0'
        },
        false: {
            left: '-60%'
        }
    }
    return (
        <>
            <div className="bars" style={expanded ? { left: '60%' } : { left: '5%' }} onClick={() => setExpaned(!expanded)}>
                <UilBars />
            </div>
            <motion.div className='sidbar'
                variants={sidebarVariants}
                animate={window.innerWidth <= 768 ? `${expanded}` : ''}
            >
                <div className='logo'>
                    <img src={logo}></img>
                    <span>Sh<span>o</span>ps</span>
                </div>
                <div className='menu'>
                    {SideData.map((item, index) => {
                        return (
                            <div
                                kay={index}
                                className={selected === index ? 'menuItem active' : 'menuItem'}
                                onClick={() => setSelected(index)}
                            >
                                <div>
                                    < item.icon />
                                </div>
                                <span>
                                    {item.heading}
                                </span>
                            </div>
                        )
                    })}
                    <div className='menuItem' onClick={() => setExpaned(!expanded)}>
                        <div>
                            < UilSignOutAlt />
                        </div>

                    </div>
                </div>
            </motion.div>
        </>
    )
}

export default Sidbar
