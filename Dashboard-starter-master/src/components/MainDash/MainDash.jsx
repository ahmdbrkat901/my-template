import React from 'react'
import Cards from './Cards/cards' 
import BasicTable from './Tabel/Table'
import './MainDash.css'

const MainDash = () => {
  return (
      <div className='MainDash'>
          <h1>Dashboard</h1>
      <Cards />
      <BasicTable/>
    </div>
  )
}

export default MainDash
