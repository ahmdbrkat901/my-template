import { motion,AnimateSharedLayout } from 'framer-motion';
import React, { useState } from 'react'
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import './card.css'
import { UilTimes } from '@iconscout/react-unicons'
import Chart from 'react-apexcharts'

const Card = (props) => {
    const [expanded, setExpanded] = useState(false);
    return (
        <AnimateSharedLayout >
            {
                expanded ?
                    (
                        < ExpandedCard  props={props} setExpanded={() => setExpanded(false)} />
                    )
                    : <CompactCard  props={props} setExpanded={() => setExpanded(true)} />
            }
        </AnimateSharedLayout>
    )
}
// CompactCard Function 
function CompactCard({ props, setExpanded }) {
    const Png = props.png
    return (
        <motion.div 
            title="Click here"
            layoutId='expandableCard'
            className='CompactCard'
            style={{
                background: props.color.backGround,
                boxShadow: props.color.boxShadow
            }} onClick={setExpanded}>
            <div className="radialBar">
                <CircularProgressbar value={props.barValue} text={`${props.barValue}%`} />
                <span>{props.title}</span>
            </div>
            <div className="detail">
                <Png />
                <span>{props.value}</span>
                <span>Last 24 Hours</span>
            </div>
        </motion.div>
    )
}
// ExpandedCard Function
function ExpandedCard({ props, setExpanded }) {
    const data = {
        options: {
            chart: {
                type: "area",
                height: "auto",
            },

            dropShadow: {
                enabled: false,
                enabledOnSeries: undefined,
                top: 0,
                left: 0,
                blur: 3,
                color: "#000",
                opacity: 0.35,
            },

            fill: {
                colors: ["#fff"],
                type: "gradient",
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                curve: "smooth",
                colors: ["white"],
            },
            tooltip: {
                x: {
                    format: "dd/MM/yy HH:mm",
                },
            },
            grid: {
                show: true,
            },
            xaxis: {
                type: "datetime",
                categories: [
                    "2018-09-19T00:00:00.000Z",
                    "2018-09-19T01:30:00.000Z",
                    "2018-09-19T02:30:00.000Z",
                    "2018-09-19T03:30:00.000Z",
                    "2018-09-19T04:30:00.000Z",
                    "2018-09-19T05:30:00.000Z",
                    "2018-09-19T06:30:00.000Z",
                ],
            },
        },
    };
    return (
        <motion.div className='ExpandedCard' style={{
            background: props.color.backGround,
            boxShadow: props.color.boxShadow
        }}
        layoutId='expandableCard'>
            <div className='icon-close' >
                <UilTimes onClick={setExpanded} />
            </div>
            <span>{props.title}</span>
            <div className="chartContainer">
                <Chart series={props.series} type='area' options={data.options} />
            </div>
            <span>Last 24 Hours</span>


        </motion.div>
    )
}



export default Card
