import './App.css'
import MainDash from './components/MainDash/MainDash';
import Sidbar from './components/Sidbar/sidbar';
import RightSide from './components/RightSide/RightSide';

function App() {
  return (
    <div className="App">
      <div className='AppGlass'>
        <Sidbar /> 
        <MainDash />
        <RightSide/>
       </div>
    </div>
  );
}

export default App;
