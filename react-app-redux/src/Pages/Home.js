import React, { Component } from "react";
class Home extends Component {
    render() {
        return (
            <div className="container mydiv"> 
                <h2>Home Products</h2>
                <br />
                <br />
                <div className="row mt-4">
                    <div className="col-md-4">
                        <div className="bbb_deals">
                            <div className="ribbon ribbon-top-right"><div><small className="cross">x </small>4</div></div>
                            <div className="bbb_deals_title">Today's Combo Offer</div>
                            <div className="bbb_deals_slider_container">
                                <div className=" bbb_deals_item">
                                    <div className="bbb_deals_image"><img src="https://i.imgur.com/9UYzfny.png" width={100+'%'} alt=""/></div>
                                    <div className="bbb_deals_content">
                                        <div className="bbb_deals_info_line d-flex flex-row justify-content-start">
                                            <div className="bbb_deals_item_category"><a href="#">Laptops</a></div>
                                            <div className="bbb_deals_item_price_a ml-auto"><strike>₹30,000</strike></div>
                                        </div>
                                        <div className="bbb_deals_info_line d-flex flex-row justify-content-start">
                                            <div className="bbb_deals_item_name">HP Notebook</div>
                                            <div className="bbb_deals_item_price ml-auto">₹25,550</div>
                                        </div>
                                        <div className="available">
                                            <div className="available_line d-flex flex-row justify-content-start">
                                                <div className="available_title">Available: <div>6</div></div>
                                                <div className="sold_stars ml-auto"> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> </div>
                                            </div>
                                            <div className="available_bar"><div style={{ width: 17  }}></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="bbb_deals">
                            <div className="ribbon ribbon-top-right"><div><small className="cross">x </small>2</div></div>
                            <div className="bbb_deals_title">Today's Combo Offer</div>
                            <div className="bbb_deals_slider_container">
                                <div className=" bbb_deals_item">
                                    <div className="bbb_deals_image"><img width={100 + '%'} src="https://i.imgur.com/9UYzfny.png" alt=""/></div>
                                    <div className="bbb_deals_content">
                                        <div className="bbb_deals_info_line d-flex flex-row justify-content-start">
                                            <div className="bbb_deals_item_category"><a href="#">Laptops</a></div>
                                            <div className="bbb_deals_item_price_a ml-auto"><strike>₹40,000</strike></div>
                                        </div>
                                        <div className="bbb_deals_info_line d-flex flex-row justify-content-start">
                                            <div className="bbb_deals_item_name">HP Envy</div>
                                            <div className="bbb_deals_item_price ml-auto">₹35,550</div>
                                        </div>
                                        <div className="available">
                                            <div className="available_line d-flex flex-row justify-content-start">
                                                <div className="available_title">Available: <div>6</div></div>
                                                <div className="sold_stars ml-auto"> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> </div>
                                            </div>
                                            <div className="available_bar"><div style={{ width: 17 }}></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="bbb_deals">
                            <div className="ribbon ribbon-top-right"><div><small className="cross">x </small>3</div></div>
                            <div className="bbb_deals_title">Today's Combo Offer</div>
                            <div className="bbb_deals_slider_container">
                                <div className=" bbb_deals_item">
                                    <div className="bbb_deals_image"><img width={100 + '%'} src="https://i.imgur.com/9UYzfny.png" alt=""/></div>
                                    <div className="bbb_deals_content">
                                        <div className="bbb_deals_info_line d-flex flex-row justify-content-start">
                                            <div className="bbb_deals_item_category"><a href="#">Laptops</a></div>
                                            <div className="bbb_deals_item_price_a ml-auto"><strike>₹30,000</strike></div>
                                        </div>
                                        <div className="bbb_deals_info_line d-flex flex-row justify-content-start">
                                            <div className="bbb_deals_item_name">Toshiba B77</div>
                                            <div className="bbb_deals_item_price ml-auto">₹27,550</div>
                                        </div>
                                        <div className="available">
                                            <div className="available_line d-flex flex-row justify-content-start">
                                                <div className="available_title">Available: <div>6</div></div>
                                                <div className="sold_stars ml-auto"> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> <i className="fa fa-star" style={{ color: 'gold' }}></i> </div>
                                            </div>
                                            <div className="available_bar"><div style={{ width: 17+'%' }}></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Home