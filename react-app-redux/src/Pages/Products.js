import React, { Component } from "react";
import Product from "../Components/Product";
import { getAll } from "../API/products";
class Products extends Component {
    state = {
        products: []
    }
    componentDidMount() {
        getAll().then(data => {
            this.setState({
                products: data
            })
        })
    }
    render() {
        const product = this.state.products

        return (
            <div>
                <h1>Products</h1>
                <div className="row">
                    {product.map((product) => <Product key={product.id} products={product} />)}
                </div>
            </div>
        )
    }
}
export default Products