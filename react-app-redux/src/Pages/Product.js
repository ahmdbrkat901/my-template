import React, { Component } from "react";
import { connect } from "react-redux";
import { getId } from "../API/products";
import {addToCart} from "../Store/Action/Action";
class Product extends Component {
    state = {
        products: [],
        quantity:1
    } 
    handelQuantity = (e) => {
        const value = e.target.value;
        if (value <= 0)
            return; 
        this.setState({
            quantity: value
        })
    }
    componentDidMount() {
        const id = this.props.match.params.id;
        getId(id).then(data =>
            this.setState({
                products: data
            }))
    } 
    addToCart=(product)=> {
        this.props.addToCart(product, this.state.quantity)
    }
    render() {
        const product = this.state.products
        const quantity= this.state.quantity
        return (
            <div className="row">
                <div className="col-12 col-md-6 mb-4"> 
                    <img className="card-img-top" src={product.image} style={{ maxHeight: 385 + 'px' }} alt="Card image cap" />
                </div>
                <div className="col-12 col-md-6">
                    <h3>{ product.name }</h3>
                    <h5 className="my-3">Price : {product.price} $ </h5>
                    <p className="my-2">{product.description}</p>
                    <h4 className="my-3">Totle : {product.price * quantity}</h4>
                    <input type="number" onChange={this.handelQuantity} value={quantity }/>
                    <button className="my-4 btn btn-primary btn-block" onClick={() => this.addToCart(product)}>Pay</button> 
                </div>
            </div>
        )
    }
} 
const mapDispatchToProps=(dispatch)=> {
    return {
        addToCart: (productInfo, quantity) => dispatch(addToCart(productInfo,quantity))
    }
}
export default connect(null,mapDispatchToProps) (Product)