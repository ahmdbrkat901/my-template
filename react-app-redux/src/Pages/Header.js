import React, { Component } from "react";
import Navbar from "../Components/Navbar";
import Router from "../Router/Route";
import { BrowserRouter, Routes, Route } from "react-router-dom";
class Header extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <Navbar />
                    <Router />
                </BrowserRouter>
            </div>
        )
    }
}
export default Header