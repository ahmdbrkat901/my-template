import React, { Component } from "react";
import { AddUser } from "../Store/Action/Action";
import { connect } from "react-redux";
import "../CSS/Login.css";
class Login extends Component {
    state = {
        nameUser: "",
        passwordUser: '',
        quantity: 1
    }
    handelUserName = (e) => {
        const value = e.target.value;
        this.setState({
            nameUser: value
        })
    }
    handelPassword = (e) => {
        const value = e.target.value;
        this.setState({
            passwordUser: value
        })
    }
    addUser = () => {
        localStorage.setItem('nameUser', this.state.nameUser)
        localStorage.setItem('passwordUser', this.state.passwordUser)
        this.setState({
            nameUser: '',
            passwordUser: ''
        })
    }

    render() {
        const nameUser = localStorage.getItem('nameUser')
        return (
            <div className="wrapper fadeInDown">
                {nameUser !== null ? <h2 className="h2"> <span className="username">{nameUser}</span> already have an account</h2> :
                    <div id="formContent">

                        <div className="fadeIn first">
                        </div>

                        <form className="mt-4">
                            <input type="text" id="login" className="fadeIn second" onChange={this.handelUserName} value={this.state.nameUser} name="login" placeholder="login" />
                            <input type='password' id="password" className="fadeIn third" onChange={this.handelPassword} value={this.state.passwordUser} name="login" placeholder="password" />
                            <input type="button" onClick={this.addUser} className="fadeIn fourth" value="Log In" />
                        </form>

                        <div id="formFooter">
                            <a className="underlineHover" href="#">Forgot Password?</a>
                        </div>

                    </div>
                }
            </div>
        )
    }
}
export default Login