import React, { Component } from "react";
import Cart from "../Components/Cart";
import { connect } from "react-redux"; 
import {clearCart}from"../Store/Action/Action"
class Carts extends Component { 
    clearCart = () => {
        this.props.clearCart()
    }
    render() {
        return (
            <div >
                <h1 className="">Carts</h1>
                <div className="row ">
                    {this.props.cartItem.map((item,index) =>
                        <Cart product={item} index={index}  key={item.products.id} />
                    )}
                </div>
                <div className="row justify-content-center">
                    <h2>Total : {this.props.quantity} $ </h2>
                    <button className="btn btn-primary btn-block my-2" onClick={this.clearCart}>Plase Order <i className="fa fa-shopping-cart fa-lg" ></i></button>
                </div>
            </div>
        )
    }
}
function mapStateToPropsInfo(state) {
    return {
        cartItem: state.cart,
        quantity: state.cart.reduce((total, item) => total + item.quantity * item.products.price, 0)
    }
} 
const mapDispatchToProps = (dispatch) => {
    return {
        clearCart: () => dispatch(clearCart())
    }
}

export default connect(mapStateToPropsInfo,mapDispatchToProps)(Carts)