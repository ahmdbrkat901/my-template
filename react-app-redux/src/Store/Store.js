import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import createReduer from "./Reducer";
function loadStorge() {
    try {
        const state = localStorage.getItem('cart')
        if (state !== null) {
            return JSON.parse(state)
        }
    } catch (error) {
        return { cart: [] }
    }
    return { cart: [] }
}
function saveState(state) {
    localStorage.setItem('cart', JSON.stringify(state))
}
// const initailState = {
//     cart: [
//         {
//             products:
//             {
//                 "id": 1,
//                 "name": "Laptop Dell",
//                 "price": 1500,
//                 "image": "/images/fimage-1.jpg",
//                 "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a"
//             }
//             ,
//             quantity: 1
//         }
//     ]
// };
const store = createStore
    (
        createReduer, loadStorge(),
        compose(
            applyMiddleware(thunk),
            window.__REDUX_DEVTOOLS_EXTENSION__
                ? window.__REDUX_DEVTOOLS_EXTENSION__()
                : f => f
        )
    );
store.subscribe(() => {
    saveState(store.getState())
})
export default store;