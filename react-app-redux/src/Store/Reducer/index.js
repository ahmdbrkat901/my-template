import { ADD_TO_CART, REMOVE_TO_CART, CLEAR_CART, LOGIN } from '../Action/Type'
export default function createReduer(state, action) {
    switch (action.type) {
        case ADD_TO_CART: {
            return {
                cart: [
                    ...state.cart,
                    {
                        products: action.productInfo,
                        quantity: action.quantity
                    }
                ]
            }
        }
        case REMOVE_TO_CART: {
            const item_index = action.index
            const newState = { ...state }
            newState.cart.splice(item_index, 1)
            return newState
        } case CLEAR_CART: {
            const newState = { ...state }
            newState.cart = []
            return newState
        }
        case LOGIN: {
            return {
                login:
                    [
                        ...state.login,
                        {
                            nameUser: action.nameUser,
                            passwordUser: action.passwordUser

                        }
                    ]
            }
        }

        default:
            return state;
    }
}
