import { ADD_TO_CART, REMOVE_TO_CART, CLEAR_CART, LOGIN } from "./Type"
export function addToCart(productInfo, quantity) {
    return {
        type: ADD_TO_CART,
        productInfo,
        quantity
    }
}
export function createRemoveToCart(index) {
    return {
        type: REMOVE_TO_CART,
        index
    }
}
export function removeToCart(index) {
    return (dispatch) => {
        dispatch(createRemoveToCart(index))
    }
}
export function clearCart() {
    return {
        type: CLEAR_CART,
    }
}
export function AddUser(nameUser, passwordUser) {
    return {
        type: LOGIN,
        nameUser,
        passwordUser
    }
}
// export function AddUser(nameUser, passwordUser) {
//     return (dispatch) => {
//         dispatch(createAddUser(nameUser, passwordUser))
//     }
// } 
