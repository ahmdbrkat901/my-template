import React, { Component } from "react";
import { removeToCart } from "../Store/Action/Action";
import { connect } from "react-redux";
import { Link } from "react-router-dom"; 
class Cart extends Component {
   
    render() {
        const product = this.props.product.products
        const quantity = this.props.product.quantity 
        const index = this.props.index
        console.log(product)
        return (
            <div className="col-12 col-lg-4 col-sm-6 col-xl-3">
                <div className="card mb-2 mb-lg-0" style={{ width: 15 + 'rem' }}>
                    <img className="card-img-top" src={product.image} alt="Card image cap" style={{ minHeight: 220 + 'px', Width: 'auto' }} />
                    <div className="card-body">
                        <h5 className="card-title">{product.name}</h5>
                        <p className="card-text">Price : {product.price} $ </p>
                        <p className="card-text">quantity : {quantity} </p>
                        <p className="card-text">Total : {product.price * quantity } $ </p>
                        <Link to={'Product/' + product.id} className="mr-4 btn btn-success">Edit  <i className="ml-1 fa fa-edit"></i></Link>
                        <button className="btn btn-danger" onClick={()=>this.props.removeToCart(index)}>Delete <i className="ml-1 fa fa-trash"></i> </button>
                    </div>
                </div>
            </div>
        )
    }
} 

export default connect(null, {removeToCart})(Cart)