import React, { Component } from "react"; 
import { Link } from "react-router-dom";

function Products(props) { 
    const product=props.products
    return (
        <div className="col-12 col-xl-4 col-md-6">

            <div className="card my-4" style={{ width: 18 + 'rem' }}>
                <img className="card-img-top" src={product.image} alt="Card image cap" style={{ minHeight: 220 + 'px', Width: 'auto' }} />
                <div className="card-body">
                    <h5 className="card-title">{product.name}</h5>
                    <p className="card-text">Price : { product.price } $ </p>
                    <Link to={'Product/' + product.id} className="btn btn-primary">Details</Link>
                </div>
            </div>
        </div>
    )
}
export default Products