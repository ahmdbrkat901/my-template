import React from "react";   
import { Outlet, Link } from "react-router-dom"; 
import { connect } from "react-redux";
function Icon(props) {
    return (
        <Link to="/Cart"> 
            <i className="fa fa-shopping-cart fa-2x" style={{ color: 'black' }}></i>
            <span className="badge badge-danger">{props.quantity}</span>
        </Link>
    )
}  
function mapStateToProps(state) {
    return { quantity: state.cart.reduce((total,item)=>total + parseInt( item.quantity),0 ) } 
}  
export default connect(mapStateToProps)(Icon)