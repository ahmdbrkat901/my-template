import React, { Component } from "react";
import { Outlet, Link } from "react-router-dom";
import Icon from "./Icon/Icon";
class Navbar extends Component {
    render() {
        const nameUser = localStorage.getItem('nameUser')
        return (
            <div className="bg-light mb-4">
                <nav className="navbar navbar-expand-lg navbar-light bg-light container">
                    <a className="navbar-brand" href="#">New Laptops</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/Products">Products</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/Cart">Cart</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/Login">
                                    {nameUser !== null ?
                                        <div>
                                            <span> Hello </span>
                                            <span className="nameUser">{nameUser}</span>
                                        </div>
                                        : 'Pleas login'
                                    }
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <Icon />
                </nav>
            </div>
        )
    }
}
export default Navbar