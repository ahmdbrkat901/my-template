import { BrowserRouter, Routes, Route } from "react-router-dom";
import React, { Component } from "react";
import Home from "../Pages/Home"
import Cart from '../Pages/Cart'
import Products from '../Pages/Products'
import Product from '../Pages/Product'
import Login from "../Pages/Login";
function Router() {
    return (
        <div className="App container" >
            <Route path="/" exact component={Home} />
            <Route path="/Cart" component={Cart} />
            <Route exact path="/Products" component={Products} />
            <Route path="/Product/:id" component={Product} />
            <Route  path="/Login" component={Login} />
        </div>
    );
}
export default Router