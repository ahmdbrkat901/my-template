import Products from "./Products.json"
export function getAll() {
    return Promise.resolve(Products)
}
export function getId(id) {
    const product = Products.find(item => item.id == id)
    return (
        Promise.resolve(product)
    )
}